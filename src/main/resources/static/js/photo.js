$(document).ready(function () {


    //nav
    var oH2 = document.getElementById("mnavh");
    var oUl = document.getElementById("starlist");
    oH2.onclick = function () {
        var style = oUl.style;
        style.display = style.display == "block" ? "none" : "block";
        oH2.className = style.display == "block" ? "open" : ""
    }

    var obj = null;
    var As = document.getElementById('starlist').getElementsByTagName('a');
    obj = As[0];
    for (i = 1; i < As.length; i++) {
        if (window.location.href.indexOf(As[i].href) >= 0)
            obj = As[i];
    }
    obj.id = 'selected';

    /*

    search

    */
    $('.search_ico').click(function () {
        $('.search_bar').toggleClass('search_open');
        if ($('#keyboard').val().length > 2) {
            $('#keyboard').val('');
            $('#searchform').submit();
        } else {
            return false;
        }
    });


    //header
    var new_scroll_position = 0;
    var last_scroll_position;
    var header = document.getElementById("header");

    window.addEventListener('scroll', function (e) {
        last_scroll_position = window.scrollY;

        if (new_scroll_position < last_scroll_position && last_scroll_position > 80) {
            header.classList.remove("slideDown");
            header.classList.add("slideUp");

        } else if (new_scroll_position > last_scroll_position) {
            header.classList.remove("slideUp");
            header.classList.add("slideDown");
        }

        new_scroll_position = last_scroll_position;
    });

    //scroll to top
    var offset = 300,
        offset_opacity = 1200,
        scroll_top_duration = 700,
        $back_to_top = $('.cd-top');

    $(window).scroll(function () {
        ($(this).scrollTop() > offset) ? $back_to_top.addClass('cd-is-visible') : $back_to_top.removeClass('cd-is-visible cd-fade-out');
        if ($(this).scrollTop() > offset_opacity) {
            $back_to_top.addClass('cd-fade-out');
        }
    });
    $back_to_top.on('click', function (event) {
        event.preventDefault();
        $('body,html').animate({
                scrollTop: 0,
            }, scroll_top_duration
        );
    });


    $('.fenlei li').click(function () {
        $(this).addClass('flselect').siblings().removeClass('flselect');
        $('.mbans>div:eq(' + $(this).index() + ')').show().siblings().hide();
    });


});

function checkForm() {
    var formData = new FormData($("#studentAddForm")[0]);
    var type = $('#photoType').val();
    formData.set("photoType", type);
    $.ajax({
        url: "/fileUpload",
        type: 'POST',
        data: formData,
        cache: false,
        async: false,
        processData: false,  //必须false才会避开jQuery对 formdata 的默认处理
        contentType: false,  //必须false才会自动加上正确的Content-Type

        success: function (data) {
            alert("成功");
            /*
            layer.alert("增加成功", {icon: 6}, function () {
                        window.parent.location.reload(); //刷新父页面
                        // 获得frame索引
                        var index = parent.layer.getFrameIndex(window.name);
                        //关闭当前frame
                        parent.layer.close(index);
                    });
            */
        },
        error: function (data) {
            alert("失败");
            /*
            layer.msg(data.message, {time: 2000});
            */
        }
    });
}