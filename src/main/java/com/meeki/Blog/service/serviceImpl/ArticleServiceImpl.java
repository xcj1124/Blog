package com.meeki.Blog.service.serviceImpl;

import com.meeki.Blog.entity.ArticleEntity;
import com.meeki.Blog.mapper.ArticleMapper;
import com.meeki.Blog.service.ArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @program: Blog
 * @description: 文章服务层接口实现类
 * @author: Mr.Xie
 * @create: 2020-05-17 13:41
 **/
@Service
public class ArticleServiceImpl implements ArticleService {
    @Autowired
    private ArticleMapper articleMapper;

    @Override
    public List<ArticleEntity> getArticleForFlagToEight() {
        return this.articleMapper.getArticleForFlagToEight();
    }

    @Override
    public List<ArticleEntity> getArticleForHitsToEight() {
        return this.articleMapper.getArticleForHitsToEight();
    }

    @Override
    public List<ArticleEntity> getArticleForTimeToEight() { //获取最新的八篇文章
        return this.articleMapper.getArticleForTimeToEight();
    }

    @Override
    public ArticleEntity getArticleFromId(Integer id) {
        return this.articleMapper.getArticleFromId(id);
    }

    @Override
    public int selectArticleCount() {
        return this.articleMapper.selectArticleCount();
    }

    @Override
    public int selectMaxID() {
        return this.articleMapper.selectMaxID();
    }

    @Override
    public List<ArticleEntity> getArticleNextAndLast(Integer id) {
        return this.articleMapper.getArticleNextAndLast(id);
    }

    @Override
    public List<ArticleEntity> getHomPageCarousel() {
        return this.articleMapper.getHomPageCarousel();
    }
}
