package com.meeki.Blog.service.serviceImpl;

import com.meeki.Blog.mapper.VisitsMapper;
import com.meeki.Blog.service.VisitsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * @program: Blog
 * @description:
 * @author: Mr.Xie
 * @create: 2020-05-22 21:18
 **/
@Service
public class VisitsServiceImpl implements VisitsService {
    @Autowired
    private VisitsMapper visitsMapper;

    @Override
    public int addVisits(Date date) {
        return this.visitsMapper.addVisits(date);
    }
}
