package com.meeki.Blog.service.serviceImpl;

import com.meeki.Blog.entity.UserEntity;
import com.meeki.Blog.mapper.UserMapper;
import com.meeki.Blog.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @program: Blog
 * @description: 个人信息服务层
 * @author: Mr.Xie
 * @create: 2020-06-02 21:22
 **/
@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserMapper userMapper;

    @Override
    public UserEntity getUserMessage(Integer id) {
        return this.userMapper.getUserMessage(id);
    }
}
