package com.meeki.Blog.service.serviceImpl;

import com.meeki.Blog.entity.PhotoEntity;
import com.meeki.Blog.mapper.PhotoMapper;
import com.meeki.Blog.service.PhotoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @program: Blog
 * @description: 照片服务层
 * @author: Mr.Xie
 * @create: 2020-06-06 12:35
 **/
@Service
public class PhotoServiceImpl implements PhotoService {
    @Autowired
    private PhotoMapper photoMapper;

    @Override
    public int addPhoto(PhotoEntity photoEntity) {
        return this.photoMapper.addPhoto(photoEntity);
    }

    @Override
    public List<PhotoEntity> getPhotoByType(String type) {
        return this.photoMapper.getPhotoByType(type);
    }

    @Override
    public List<PhotoEntity> getPhotoNowFour() {
        return this.photoMapper.getPhotoNowFour();
    }
}
