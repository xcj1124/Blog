package com.meeki.Blog.service.serviceImpl;

import com.meeki.Blog.entity.TimeEntity;
import com.meeki.Blog.mapper.TimeMapper;
import com.meeki.Blog.service.TimeService;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @program: Blog
 * @description: 时间轴服务层
 * @author: Mr.Xie
 * @create: 2020-06-05 21:51
 **/
@Service
public class TimeServiceImpl implements TimeService {
    @Autowired
    TimeMapper timeMapper;

    @Override
    public int getAllTime() {
        return this.timeMapper.getAllTime();
    }

    @Override
    public List<TimeEntity> findTimeToNum(int ns, int ps) {
        return this.timeMapper.findTimeToNum(ns, ps);
    }
}
