package com.meeki.Blog.service;

import com.meeki.Blog.entity.TimeEntity;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @program: Blog
 * @description: 时间轴服务层接口
 * @author: Mr.Xie
 * @create: 2020-06-05 21:50
 **/
public interface TimeService {
    // 查询总条数
    int getAllTime();

    // 分页查询留言
    List<TimeEntity> findTimeToNum(int ns, int ps);
}
