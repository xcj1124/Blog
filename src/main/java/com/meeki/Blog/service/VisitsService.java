package com.meeki.Blog.service;

import java.util.Date;

/**
 * @program: Blog
 * @description:
 * @author: Mr.Xie
 * @create: 2020-05-22 21:16
 **/
public interface VisitsService {
    int addVisits(Date date);  //访问量添加
}
