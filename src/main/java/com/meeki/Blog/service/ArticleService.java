package com.meeki.Blog.service;

import com.meeki.Blog.entity.ArticleEntity;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @program: Blog
 * @description: 文章的服务层接口
 * @author: Mr.Xie
 * @create: 2020-05-17 13:39
 **/

public interface ArticleService {

    List<ArticleEntity> getArticleForFlagToEight(); //获取推荐的文章

    List<ArticleEntity> getArticleForHitsToEight();  //点击量前五的文章查询

    List<ArticleEntity> getArticleForTimeToEight(); //最新八篇文章查询

    ArticleEntity getArticleFromId(Integer id);   //根据id查询文章

    int selectArticleCount();     //查询文章总数量

    int selectMaxID();     //查询ID最大值

    List<ArticleEntity> getArticleNextAndLast(Integer id);  //根据id获得当前文章的前后文章信息

    List<ArticleEntity> getHomPageCarousel(); //获取主页轮播图
}
