package com.meeki.Blog.service;

import com.meeki.Blog.entity.PhotoEntity;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @program: Blog
 * @description: 照片服务层接口
 * @author: Mr.Xie
 * @create: 2020-06-06 12:34
 **/
public interface PhotoService {
    int addPhoto(PhotoEntity photoEntity);

    List<PhotoEntity> getPhotoByType(String type);

    List<PhotoEntity> getPhotoNowFour(); //查询最新上传的五张照片
}
