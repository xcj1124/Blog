package com.meeki.Blog.service;

import com.meeki.Blog.entity.UserEntity;

/**
 * @program: Blog
 * @description: 个人信息服务层接口
 * @author: Mr.Xie
 * @create: 2020-06-02 21:20
 **/
public interface UserService {
    UserEntity getUserMessage(Integer id);
}
