package com.meeki.Blog.controller;

import com.meeki.Blog.entity.ArticleEntity;
import com.meeki.Blog.service.ArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * @program: Blog
 * @description: 文章处理器
 * @author: Mr.Xie
 * @create: 2020-05-17 13:45
 **/
@Controller
public class ArticleController {
    @Autowired
    private ArticleService articleService;

    /**
     * @Description: 根据文章ID打开文章详情页面
     * @Param: []
     * @return: java.lang.String
     * @Author: Mr.Xie
     * @Date: 2020/5/20
     */
    @RequestMapping("/articleMessage.html")
    public String goArticleMessage(HttpServletRequest request, HttpSession session, Model model) {
        int id = Integer.parseInt(request.getParameter("id"));//获取请求参数
        int count = this.articleService.selectMaxID();//获取文章id最大值
        ArticleEntity articleEntity = this.articleService.getArticleFromId(id);//根据id获取文章信息
        List<ArticleEntity> nextAndLastArticleEntity = this.articleService.getArticleNextAndLast(id);//根据id得到该id的前后两篇文章
        List<ArticleEntity> articleForHitsToEight = this.articleService.getArticleForHitsToEight();//得到最新的前八篇文章信息
        if (1 < id && id < count) {
            model.addAttribute("lastArticleEntity", nextAndLastArticleEntity.get(0));//当前id值的上一篇文章信息
            model.addAttribute("nextArticleEntity", nextAndLastArticleEntity.get(1)); //当前id值的下一篇文章信息
        } else if (id <= 1) {
            model.addAttribute("lastArticleEntity", this.articleService.getArticleFromId(id));//当前id值的上一篇文章信息
            model.addAttribute("nextArticleEntity", nextAndLastArticleEntity.get(0)); //当前id值的下一篇文章信息
        } else {
            model.addAttribute("lastArticleEntity", nextAndLastArticleEntity.get(0));//当前id值的上一篇文章信息
            model.addAttribute("nextArticleEntity", this.articleService.getArticleFromId(count)); //当前id值的下一篇文章信息
        }
        model.addAttribute("articleForHitsToEight", articleForHitsToEight);  //点击排行信息
        model.addAttribute("articleEntity", articleEntity); //当前id值文章信息
        return "articleMessage";
    }
}
