package com.meeki.Blog.controller;

import com.meeki.Blog.entity.PhotoEntity;
import com.meeki.Blog.service.PhotoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.UUID;

/**
 * @program: Blog
 * @description: 图片控制层
 * @author: Mr.Xie
 * @create: 2020-06-04 22:08
 **/
@Controller
public class PhotoController {
    private static final Logger logger = LoggerFactory.getLogger(PhotoController.class);
    @Autowired
    private PhotoService photoService;

    @RequestMapping("/photo.html")
    public String photo(Model model) {
        List<PhotoEntity> photoEntityList = this.photoService.getPhotoNowFour();//获取最新四张照片展示在图片集锦
        model.addAttribute("fivePhoto",photoEntityList);
        return "photo";
    }

    //照片首页
    @RequestMapping("/test.html")
    public String test() {
        return "test";
    }

    //上传照片
    @PostMapping(value = "/fileUpload")
    @ResponseBody
    public String fileUpload(@RequestParam(value = "file") MultipartFile file, PhotoEntity photoEntity, HttpServletRequest request) {
        if (file.isEmpty()) {
            logger.info("文件为空");
        }
        String fileName = file.getOriginalFilename();  // 文件名
        String suffixName = fileName.substring(fileName.lastIndexOf("."));  // 后缀名
        String filePath = "D://IdeaProject//Blog//src//main//resources//static//images//"; // 上传后的路径
        fileName = UUID.randomUUID().toString().replace("-", "") + suffixName; // 新文件名
        File dest = new File(filePath + fileName);
        if (!dest.getParentFile().exists()) {
            dest.getParentFile().mkdirs();
        }
        try {
            file.transferTo(dest);
        } catch (IOException e) {
            logger.info("文件上传异常");
        }

        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd :hh:mm:ss");
        PhotoEntity entity = new PhotoEntity();

        entity.setPhotoTime(dateFormat.format(calendar.getTime()));
        entity.setPhotoName(photoEntity.getPhotoName());
        entity.setPhotoType(photoEntity.getPhotoType());
        entity.setPhotoUrl("/static/images/"+fileName);
        logger.info("照片上传时间：" + dateFormat.format(calendar.getTime()));
        logger.info("照片分类：" + photoEntity.getPhotoType());
        logger.info("照片名：" + photoEntity.getPhotoName());
        logger.info("照片文件名：" + fileName);
        this.photoService.addPhoto(entity);
        return "ok";
    }
}
