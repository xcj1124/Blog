package com.meeki.Blog.controller;

import com.meeki.Blog.entity.ArticleEntity;
import com.meeki.Blog.entity.UserEntity;
import com.meeki.Blog.service.ArticleService;
import com.meeki.Blog.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * @program: Blog
 * @description: 主页控制层
 * @author: Mr.Xie
 * @create: 2020-06-01 21:49
 **/
@Controller
public class IndexController {
    @Autowired
    private ArticleService articleService; //文章信息服务接口
    @Autowired
    private UserService userService;  //个人信息服务层

    /**
     * @Description: 跳转到主页并展示文章信息
     * @Param: []
     * @return: java.lang.String
     * @Author: Mr.Xie
     * @Date: 2020/5/13
     */
    @RequestMapping("/index.html")
    public String goIndex(HttpServletRequest request, HttpSession session, Model model) {
        //获取用户信息
        UserEntity userEntity=this.userService.getUserMessage(1);
        //获取主页轮播图
        List<ArticleEntity> homPageCarouselFlag = this.articleService.getHomPageCarousel();
        //得到推荐的文章信息
        List<ArticleEntity> articleForFlagToEight = this.articleService.getArticleForFlagToEight();
        //得到最新的前八篇文章信息
        List<ArticleEntity> articleForTimeToEight = this.articleService.getArticleForTimeToEight();
        model.addAttribute("userEntity",userEntity);  //用户信息储存到model
        session.setAttribute("userSession",userEntity);  //用户信息存入session
        model.addAttribute("homPageCarouselFlag", homPageCarouselFlag); //获取轮播图并储存
        if (articleForFlagToEight.size() > 1) {
            List<ArticleEntity> articleForFlagToTop = articleForFlagToEight.subList(0, articleForFlagToEight.size() / 2);//获取推荐文章的前一部分
            List<ArticleEntity> articleForFlagToBottom = articleForFlagToEight.subList(articleForFlagToEight.size() / 2, articleForFlagToEight.size());//获取推荐文章的后一部分
            model.addAttribute("articleForFlagToTop", articleForFlagToTop); //推荐文章的前一半
            model.addAttribute("articleForFlagToBottom", articleForFlagToBottom); //推荐文章的后一半
            session.setAttribute("articleForFlagToTopSession", articleForFlagToTop);//推荐文章的前一半存入session
            session.setAttribute("articleForFlagToBottomSession", articleForFlagToBottom);//推荐文章的后一半存入session
        } else {
            model.addAttribute("articleForFlagToTop", articleForFlagToEight); //推荐文章的前一半
            model.addAttribute("articleForFlagToBottom", articleForFlagToEight); //推荐文章的后一半
            session.setAttribute("articleForFlagToTopSession", articleForFlagToEight);//推荐文章的前一半存入session
            session.setAttribute("articleForFlagToBottomSession", articleForFlagToEight);//推荐文章的后一半存入session
        }
        model.addAttribute("articleForFlagToEight", articleForFlagToEight); //返回推荐文章的前八篇
        model.addAttribute("articleForTimeToEight", articleForTimeToEight);//返回最新的八篇文章
        session.setAttribute("articleForTimeToEightSession", articleForTimeToEight);//返回最新的八篇文章存入session
        return "index";
    }
}
