package com.meeki.Blog.controller;

import com.meeki.Blog.entity.UserEntity;
import com.meeki.Blog.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * @program: Blog
 * @description: 个人信息控制层
 * @author: Mr.Xie
 * @create: 2020-06-02 21:24
 **/
@Controller
public class UserController {
    @Autowired
    private UserService userService;

    /**
     * @Description: 跳转到关于我
     * @Param: []
     * @return: java.lang.String
     * @Author: Mr.Xie
     * @Date: 2020/5/15
     */
    @RequestMapping("/about.html")
    public String About(HttpServletRequest request, HttpSession session, Model model) {
        UserEntity userEntity = (UserEntity) session.getAttribute("userSession");
        model.addAttribute("userEntitySession", userEntity);
        return "about";
    }
}
