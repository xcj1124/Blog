package com.meeki.Blog.controller;

import com.meeki.Blog.entity.VisitsEntity;
import com.meeki.Blog.service.VisitsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @program: Blog
 * @description: 访问量统计
 * @author: Mr.Xie
 * @create: 2020-05-22 20:55
 **/
@RestController
public class VisitController {
    @Autowired
    private VisitsService visitsService;
    @RequestMapping("/addVisits")
    public String addVisits(){   //访问量添加
        Date date=new Date();
        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-mm-dd");
        sdf.format(date);
        System.out.println("当前时间："+date.toString());
        int temp=this.visitsService.addVisits(date);
        System.out.println("结果： "+temp);
        return "ok";
    }
}
