package com.meeki.Blog.controller;

import com.meeki.Blog.entity.TimeEntity;
import com.meeki.Blog.entity.PageEntity;
import com.meeki.Blog.service.TimeService;
import com.meeki.Blog.unit.GetUrl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @program: Blog
 * @description: 时间轴控制层
 * @author: Mr.Xie
 * @create: 2020-06-05 21:08
 **/
@Controller
public class TimeController {
    @Autowired
    private TimeService timeService;

    @RequestMapping("/time.html")
    public String time(Model model, HttpServletRequest request) {
// 获取请求的参数pc
        String cp = request.getParameter("cp");
        // 总记录数
        int sum = this.timeService.getAllTime();
        // 获得请求URL
        String url = GetUrl.getUrl(request);
        // 创建page对象
        PageEntity<TimeEntity> listPage = new PageEntity<TimeEntity>();
        // 创建Time集合对象
        List<TimeEntity> adc = null;
        // 赋值总记录数
        listPage.setTs(sum);
        // 设置url
        listPage.setUrl(url);
        // 设置每页显示的条数
        listPage.setPs(24);
        if (cp == null || cp.trim().isEmpty()) {
            listPage.setCp(1);
        } else {
            listPage.setCp(Integer.parseInt(cp));
        }
        adc = this.timeService.findTimeToNum(listPage.getNs(),
                listPage.getPs());
        listPage.setList(adc);
        for (TimeEntity abc : adc) {
            System.out.println("名字：" + abc.getName());
        }
        model.addAttribute("lp", listPage);
        model.addAttribute("adc", adc);
        return "time";
    }
}
