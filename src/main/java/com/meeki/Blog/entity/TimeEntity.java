package com.meeki.Blog.entity;

/**
 * @program: Blog
 * @description: 时间轴
 * @author: Mr.Xie
 * @create: 2020-06-05 21:42
 **/
public class TimeEntity {
    private int id;
    private String name;
    private String creatdate;

    public TimeEntity() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCreatdate() {
        return creatdate;
    }

    public void setCreatdate(String creatdate) {
        this.creatdate = creatdate;
    }
}
