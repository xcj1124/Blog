package com.meeki.Blog.entity;

/**
 * @program: Blog
 * @description: 个人信息
 * @author: Mr.Xie
 * @create: 2020-06-02 21:10
 **/
public class UserEntity {
    private int id;
    private String name;  //姓名
    private int age;   //年龄
    private String birther; //生日
    private String job;  //工作
    private String email;  //电子邮箱
    private String wechar;  //微信
    private String like;  //爱好
    private String personalprofile;   //个人简介
    private String portrait;  //头像

    public UserEntity() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getBirther() {
        return birther;
    }

    public void setBirther(String birther) {
        this.birther = birther;
    }

    public String getJob() {
        return job;
    }

    public void setJob(String job) {
        this.job = job;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getWechar() {
        return wechar;
    }

    public void setWechar(String wechar) {
        this.wechar = wechar;
    }

    public String getLike() {
        return like;
    }

    public void setLike(String like) {
        this.like = like;
    }

    public String getPersonalprofile() {
        return personalprofile;
    }

    public void setPersonalprofile(String personalprofile) {
        this.personalprofile = personalprofile;
    }

    public String getPortrait() {
        return portrait;
    }

    public void setPortrait(String portrait) {
        this.portrait = portrait;
    }
}
