package com.meeki.Blog.entity;

/**
 * @program: Blog
 * @description: 访问量对应的实体层
 * @author: Mr.Xie
 * @create: 2020-05-22 21:11
 **/
public class VisitsEntity {
    private int id;
    private String visitsDate;

    public VisitsEntity() {
    }

    public String getVisitsDate() {
        return visitsDate;
    }

    public void setVisitsDate(String visitsDate) {
        this.visitsDate = visitsDate;
    }
}
