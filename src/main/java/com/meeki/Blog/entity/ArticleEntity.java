package com.meeki.Blog.entity;

/**
 * @program: Blog
 * @description: 文章实体类
 * @author: Mr.Xie
 * @create: 2020-05-16 22:55
 **/
public class ArticleEntity {
    private int id;  //文章编号（主键ID）
    private String articleTitle; //文章标题
    private String pictureURL;   //文章对应的封面URL路径
    private String linkAddress;  //文章对应的URL路径
    private String articleContent; //文章的内容
    private String releaseTime;  //文章发布时间
    private String author;  //文章的作者
    private String hits;  //文章点击量
    private int tjFlag;   //是否是推荐标识
    private String articleIntroduction;  //文章简介（自动截取文章内容的前一百字）
    private int homPageCarousel;  //是否作为轮播图标识（1标识是，0标识否）

    public ArticleEntity() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getArticleTitle() {
        return articleTitle;
    }

    public void setArticleTitle(String articleTitle) {
        this.articleTitle = articleTitle;
    }

    public String getPictureURL() {
        return pictureURL;
    }

    public void setPictureURL(String pictureURL) {
        this.pictureURL = pictureURL;
    }

    public String getLinkAddress() {
        return linkAddress;
    }

    public void setLinkAddress(String linkAddress) {
        this.linkAddress = linkAddress;
    }

    public String getArticleContent() {
        return articleContent;
    }

    public void setArticleContent(String articleContent) {
        this.articleContent = articleContent;
    }

    public String getReleaseTime() {
        return releaseTime;
    }

    public void setReleaseTime(String releaseTime) {
        this.releaseTime = releaseTime;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getHits() {
        return hits;
    }

    public void setHits(String hits) {
        this.hits = hits;
    }

    public int getTjFlag() {
        return tjFlag;
    }

    public void setTjFlag(int tjFlag) {
        this.tjFlag = tjFlag;
    }

    public String getArticleIntroduction() {
        return articleIntroduction;
    }

    public void setArticleIntroduction(String articleIntroduction) {
        if (this.articleContent.length() > 100) {
            this.articleIntroduction = this.articleContent.substring(0, 100) + "...";
        } else {
            this.articleIntroduction = this.articleContent.substring(0, 1) + "...";
        }

    }

    public int getHomPageCarousel() {
        return homPageCarousel;
    }

    public void setHomPageCarousel(int homPageCarousel) {
        this.homPageCarousel = homPageCarousel;
    }
}

