package com.meeki.Blog.entity;

/**
 * @program: Blog
 * @description: 照片实体层
 * @author: Mr.Xie
 * @create: 2020-06-06 10:46
 **/
public class PhotoEntity {
    private int id;
    private String photoName; //照片名字
    private String photoTime; //上传时间
    private String photoUrl; //图片位置
    private String photoType; //图片归类

    public PhotoEntity() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPhotoName() {
        return photoName;
    }

    public void setPhotoName(String photoName) {
        this.photoName = photoName;
    }

    public String getPhotoTime() {
        return photoTime;
    }

    public void setPhotoTime(String photoTime) {
        this.photoTime = photoTime;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public String getPhotoType() {
        return photoType;
    }

    public void setPhotoType(String photoType) {
        this.photoType = photoType;
    }
}
