package com.meeki.Blog.mapper;

import com.meeki.Blog.entity.UserEntity;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * @program: Blog
 * @description: 个人信息实体层
 * @author: Mr.Xie
 * @create: 2020-06-02 21:17
 **/
public interface UserMapper {
    @Select("SELECT * from `user` WHERE id=#{id}")
    UserEntity getUserMessage(@Param("id") Integer id);
}
