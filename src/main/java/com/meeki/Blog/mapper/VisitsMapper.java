package com.meeki.Blog.mapper;

import com.meeki.Blog.entity.VisitsEntity;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.Date;

/**
 * @program: Blog
 * @description:
 * @author: Mr.Xie
 * @create: 2020-05-22 21:16
 **/
public interface VisitsMapper {

    Integer addVisits(@Param("date") Date date);   //访问量添加
}
