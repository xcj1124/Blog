package com.meeki.Blog.mapper;

import com.meeki.Blog.entity.ArticleEntity;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @program: Blog
 * @description: 文章对应的数据访问层接口
 * @author: Mr.Xie
 * @create: 2020-05-17 13:27
 **/
public interface ArticleMapper {
    /**
     * @Description: 获取推荐的文章
     * @Param: []
     * @return: java.util.List<com.meeki.Blog.entity.ArticleEntity>
     * @Author: Mr.Xie
     * @Date: 2020/5/17
     */
    @Select("select * from article WHERE tjFlag=1 limit 8")
    List<ArticleEntity> getArticleForFlagToEight();

    /**
     * @Description: 查询点击量前五的文章加载到主页
     * @Param: []
     * @return: java.util.List<com.meeki.Blog.entity.ArticleEntity>
     * @Author: Mr.Xie
     * @Date: 2020/5/17
     */
    @Select("select * from article ORDER BY hits desc limit 8")
    List<ArticleEntity> getArticleForHitsToEight();

    /**
     * @Description: 查询最新的前八篇文章加载到主页
     * @Param: []
     * @return: java.util.List<com.meeki.Blog.entity.ArticleEntity>
     * @Author: Mr.Xie
     * @Date: 2020/5/17
     */
    @Select("select * from article ORDER BY releaseTime desc limit 8")
    List<ArticleEntity> getArticleForTimeToEight();

    /**
     * @Description: 根据ID查询文章
     * @Param: [id]
     * @return: com.meeki.Blog.entity.ArticleEntity
     * @Author: Mr.Xie
     * @Date: 2020/5/20
     */
    @Select("SELECT * from article where id=#{id}")
    ArticleEntity getArticleFromId(@Param("id") Integer id);

    /**
     * @Description:查询ID最大值
     * @Param: []
     * @return: int
     * @Author: Mr.Xie
     * @Date: 2020/5/27
     */
    @Select("SELECT MAX(id) FROM article ")
    int selectMaxID();

    /**
     * @Description: 查询文章的总数目
     * @Param: []
     * @return: int
     * @Author: Mr.Xie
     * @Date: 2020/5/21
     */
    @Select("SELECT count(1) from article")
    int selectArticleCount();

    /**
     * @Description: 查询当前文章的前后记录
     * @Param: []
     * @return: java.util.List<com.meeki.Blog.entity.ArticleEntity>
     * @Author: Mr.Xie
     * @Date: 2020/5/17
     */
    @Select("SELECT * FROM article WHERE id IN (SELECT CASE WHEN SIGN(id - #{id}) > 0 THEN MIN(id) WHEN SIGN(id - #{id}) < 0 THEN MAX(id) END AS id FROM article WHERE id <> #{id} GROUP BY SIGN(id - #{id}) ORDER BY SIGN(id - #{id})) ORDER BY id ASC")
    List<ArticleEntity> getArticleNextAndLast(@Param("id") Integer id);

    @Select("SELECT * from article t where t.homPageCarousel>0")
    List<ArticleEntity> getHomPageCarousel(); //获取主页轮播图
}
