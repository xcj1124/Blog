package com.meeki.Blog.mapper;

import com.meeki.Blog.entity.PhotoEntity;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @program: Blog
 * @description: 图片映射层
 * @author: Mr.Xie
 * @create: 2020-06-06 10:51
 **/
public interface PhotoMapper {
    int addPhoto(@Param("photoEntity") PhotoEntity photoEntity);//上传图片

    @Select("select * from photo where photoType=#{type}")
    List<PhotoEntity> getPhotoByType(@Param("type") String type); //根据类型查询图片
    @Select("select * from photo ORDER BY photoTime  LIMIT 0,4")
    List<PhotoEntity> getPhotoNowFour(); //查询最新上传的四张照片
}
