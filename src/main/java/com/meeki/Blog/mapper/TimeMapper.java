package com.meeki.Blog.mapper;

import com.meeki.Blog.entity.TimeEntity;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @program: Blog
 * @description:
 * @author: Mr.Xie
 * @create: 2020-06-05 21:48
 **/
public interface TimeMapper {
    // 查询总条数
    @Select("SELECT COUNT(1) FROM time")
    int getAllTime();

    // 分页查询留言
    @Select("SELECT * FROM time ORDER BY creatdate LIMIT #{ns},#{ps}")
    List<TimeEntity> findTimeToNum(@Param("ns") int ns, @Param("ps") int ps);
}
